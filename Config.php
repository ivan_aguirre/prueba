<?php

//Acceso a Base de Datos
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'ttt');
define('DB_CHAR', 'utf8');

//PATH'S
define('BASE_URL_SITE', 'http://localhost/sisei'); //BASE URL DEL ADMINISTRADOR
define('BASE_URL', 'http://localhost/sisei/'); //BASE URL DEL ADMINISTRADOR

define('BASE_FILE', 'C:\xampp\htdocs\sisei\upload'); 
define('BASE_FILE_URL', 'http://localhost/sisei/upload');

define('BASE_URL_IMG_PERFIL', 'http://localhost/sisei/fotos/'); //PATH subida fotos de perfil
define('BASE_FILE_IMG_PERFIL', 'C:\xampp\htdocs\sisei\fotos\\'); //URL Fotos de perfil

define('FILE_CONFIG_PLANTILLA', 'http://localhost/sisei/template/lte/configs.php'); //PATH de archivo de configuración de la plantilla

//Otras opciones
define('BASE_CONVERT', ''); //C:/ImageMagick/convert.exe
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'lte'); //twb
define('APP_NAME', 'Sistema para el Seguimiento de Incidencias ');
define('APP_SLOGAN', '');
define('APP_COMPANY', 'www.com');
define('SESSION_TIME', 120);
define('HASH_KEY', '4f6a6d832be79');

?>